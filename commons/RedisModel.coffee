"use strict"

Yoi = require "yoi"

TYPES =
  STRING  : 1
  NUMBER  : 2
  OBJECT  : 3
  DATE    : 4


class Model

  name    : ""
  fields  : {}

  get: (id) ->
    promise = new Yoi.Hope.Promise()
    Yoi.Redis.run "HGETALL", "#{@name}:#{id}", (error, result) =>
      promise.done error, result
    return promise

  getBy: (field, value) ->
    promise = new Yoi.Hope.Promise()
    if @fields[field].get is true
      Yoi.Redis.run "GET", "#{@name}:#{field}:#{value}", (error, result) =>
        @get(result).then (error, result) ->
          promise.done error, result
    else promise.done "Unable to get by #{field}", null
    return promise

  doPostFavorite: (shortcut, method) ->
    promise = new Yoi.Hope.Promise()
    Yoi.Redis.run method, "#{shortcut}:fav", (error, result) =>
      promise.done error, shortcut
    return promise

  doUserPostFavorite: (shortcut, user) ->
    promise = new Yoi.Hope.Promise()
    Yoi.Redis.run "SET", "#{user}:#{shortcut}:fav", shortcut, (error, result) =>
      promise.done error, result
    return promise

  find: (pagination) ->
    promise = new Yoi.Hope.Promise()
    start = 0
    stop = -1
    if pagination
      start = pagination.page * pagination.results
      stop = start + pagination.results - 1

    Yoi.Redis.run "ZREVRANGE", "#{@name}:__indexes__:created_at", start, stop, (error, ids) =>
      if error then promise.done error
      else
        multiTasks = (["HGETALL", "#{@name}:#{id}"] for id in ids)
        Yoi.Redis.multi multiTasks, (error, result) =>
          promise.done error, result

    return promise

  getFavorites: (user) ->
    promise = new Yoi.Hope.Promise()
    posts = []
    Yoi.Redis.run "keys", "*#{user}*fav*", (error, results) =>
      for result in results
        shortcut = result.split(":")[1]
        @getBy("shortcut", shortcut).then (error, result) ->
          posts.push result
          promise.done error, posts
    promise

  deleteFavorite: (user, shortcut) ->
    promise = new Yoi.Hope.Promise()
    Yoi.Redis.run "DEL", "#{user}:#{shortcut}:fav", shortcut, (error, result) =>
      promise.done error, result
    return promise

  create: (values={}) ->
    @model = {}
    @_updateAttributes(values)
    return @

  update: (values) ->
    @_updateAttributes(values)
    return @

  remove: (id) -> @

  save: ->
    promise = new Yoi.Hope.Promise()
    tasks = []
    now = (new Date()).getTime()
    unless @model.id
      tasks.push @_generateUID
      @model.created_at = now
    else @model.updated_at = now
    tasks.push @_save
    tasks.push @_setIndexes

    Yoi.Hope.shield(tasks).then (error, result) =>
      promise.done error, @model
    return promise

  # Private methods
  _save: =>
    promise = new Yoi.Hope.Promise()
    multiTasks = []
    multiTasks.push ["HMSET", "#{@name}:#{@model.id}", @model]
    for field in @_fields("get", true)
      multiTasks.push ["SET", "#{@name}:#{field}:#{@model[field]}", @model.id]

    Yoi.Redis.multi multiTasks, (error, result) =>
      promise.done error, @

    return promise

  _fields: (key, value) ->
    fields = []
    for field of @fields
      val = @fields[field][key]
      if val is value then fields.push(field)
    fields

  _generateUID: =>
    promise = new Yoi.Hope.Promise()
    Yoi.Redis.run "INCR", "global:uid:#{@name}", (error, result) =>
      @model.id = result
      promise.done error, result
    return promise

  _updateAttributes: (values) ->
    for field of @fields
      value = values[field]
      if value
        options = @fields[field]
        if options.type is TYPES.NUMBER
          value = parseFloat(value)
        @model[field] = value

  _setIndexes: =>
    promise = new Yoi.Hope.Promise()
    tasks = []
    @sort_fields = @sort_fields or []
    unless "created_at" in @sort_fields then @sort_fields.push("created_at")
    multiTasks = []
    for field in @sort_fields
      multiTasks.push ["ZADD", "#{@name}:__indexes__:#{field}", @model[field], @model.id]

    Yoi.Redis.multi multiTasks, (error, result) ->
      promise.done error, result

    return promise

Model.TYPES = TYPES
exports = module.exports = Model
