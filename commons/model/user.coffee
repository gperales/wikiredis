"use strict"

Yoi         = require "yoi"
RedisModel  = require "../RedisModel"
TYPE        = RedisModel.TYPES


class User extends RedisModel

  name: "user"

  fields:
    mail                  : type: TYPE.STRING
    name                  : type: TYPE.STRING
    username              : type: TYPE.STRING
    avatar                : type: TYPE.STRING
    # Appnima
    appnima_id            : type: TYPE.STRING
    appnima_access_token  : type: TYPE.STRING
    appnima_refresh_token : type: TYPE.STRING
    appnima_expires_in    : type: TYPE.STRING

  signup: (mail, password) ->

  updateAppnima: (appnima) ->
    @model.appnima_id = appnima.id
    @model.appnima_access_token = appnima.access_token
    @model.appnima_refresh_token = appnima.refresh_token
    do @save

  parse: ->
    id                    : @model.id
    mail                  : @model.mail
    name                  : @model.name
    username              : @model.username
    avatar                : @model.avatar
    appnima_id            : @model.appnima_id
    appnima_access_token  : @model.appnima_access_token
    appnima_refresh_token : @model.appnima_refresh_token
    appnima_expires_in    : @model.appnima_expires_in
    created_at            : new Date(@model.created_at)
    updated_at            : new Date(@model.updated_at)


module.exports = User
