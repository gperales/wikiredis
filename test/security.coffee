"use strict"

Yoi       = require "yoi"

module.exports = ->

  tasks = []
  for user in test.users
    tasks.push _signup(user)
    tasks.push _login(user)
  tasks

# Private methods
_signup = (user) -> ->
  Yoi.Test "POST", "api/signup", user, null, "Registrar #{user.mail} con App/nima", 409

_login = (user) -> ->
  promise = new Yoi.Hope.Promise()
  Yoi.Test "POST", "api/login", user, null, "Loguear a #{user.mail} con App/nima", 200, (response) ->
    user.id = response.user.id
    user.token = response.user.access_token
    promise.done null, response
  promise
