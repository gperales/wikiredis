module.exports = (grunt) ->

  grunt.initConfig
    pkg    : grunt.file.readJSON "package.json"
    office : grunt.file.readYAML("environments/development.yml").deploy

    meta:
      file   : 'wikiredis'
      assets : "assets",
      temp   : "build",
      banner : """
        /* <%= pkg.name %> v<%= pkg.version %> - <%= grunt.template.today("m/d/yyyy") %>
           <%= pkg.homepage %>
           Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %> - Licensed <%= _.pluck(pkg.license, "type").join(", ") %> */

        """
    # =========================================================================
    src:
      coffee: [
        "src/monocle/*.coffee",
        "src/monocle/models/*.coffee",
        "src/monocle/views/*.coffee",
        "src/monocle/controllers/*.coffee"],

      components:
        js: [
          "src/components/jquery/jquery.js",
          "src/components/monocle/monocle.js",
          "src/components/momentjs/moment.js"]

    ignore:
      shell: [
        "--exclude wikiredis/.git/",
        "--exclude wikiredis/.gitignore",
        "--exclude wikiredis/.gitmodules",
        "--exclude wikiredis/.nodemonignore",
        "--exclude wikiredis/node_modules/",
        "--exclude wikiredis/test/",
        "--exclude wikiredis/build/",
        "--exclude wikiredis/deploy",
        "--exclude wikiredis/gruntfile.coffee",
        "--exclude wikiredis/yoitest.js",
        "--exclude wikiredis/yoitest.yml"]

    # =========================================================================
    coffee:
      backend : files: "<%=meta.temp%>/<%= meta.file %>.<%=pkg.version%>.debug.js": "<%= src.coffee %>"

    uglify:
      options : compress: false, banner: "<%= meta.banner %>", mangle: false#, report: "gzip"
      backend : files: "<%=meta.assets%>/js/<%= meta.file %>.<%=pkg.version%>.js": "<%=meta.temp%>/<%= meta.file %>.<%=pkg.version%>.debug.js"

    concat:
      backend_js:
        src: "<%= src.components.js %>",  dest: "<%=meta.assets%>/js/<%= meta.file %>.components.js"


    watch:
      backend_coffee:
        files: ["<%= src.coffee %>"]
        tasks: ["coffee:backend", "uglify:backend"]

    shell:
      deploy_office:
        command: [
          "rsync -av --inplace ssh ../<%= office.folder %> root@<%= office.address.hosting_ip %>:<%= office.address.server_path %> <%= ignore.shell.join(' ') %>",
          "ssh root@<%= office.address.hosting_ip %> 'cd <%= office.address.server_path %>/<%= office.folder %> && npm install'",
          "ssh root@<%= office.address.hosting_ip %> 'cd <%= office.address.server_path %>/<%= office.folder %> && <%= office.pm2_start %>'"
        ].join(';')
        options: stdout: true

  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-shell"

  grunt.registerTask "default", [ "coffee", "uglify", "concat"]
  grunt.registerTask('deploy-office', ['shell:deploy_office'])
