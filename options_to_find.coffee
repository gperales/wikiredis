id = INCR global:uid:post
title = rest.parameter "title"
url = rest.parameter "url"
owner = rest.parameter "owner"
created_at = new Date()

HMSET post:"#{id}" title "#{title}" url "#{url}" owner "#{owner}" created_at "#{created_at}"


#Por cada post que creemos se hace:
fecha_del_post = created_at.getTime()
ZADD lista_de_posts "#{fecha_del_post}" "#{id}"

#Para obtener post entre unas determinadas fechas hacemos:
fecha_1 = 2004489498
fecha_2 = 543534543543
ZRANGEBYSCORE lista_de_posts (fecha_1 (fecha_2
#Si ponemos en ambos casos el (, significa que va a estar entre esas fechas sin contar la fecha_2
#Si ponemos (fecha_1 fecha_2 seria--> fecha_1 < POSTS <= fecha_2

#en este caso es de una fecha hasta el infinito
ZRANGEBYSCORE lista_de_posts (fecha_1 +inf

#en este caso es desde siempre hasta una fecha
ZRANGEBYSCORE lista_de_posts -inf fecha_2

#en este caso seria todas las fechas
ZRANGEBYSCORE lista_de_posts -inf +inf

