window.WikiRedis = WikiRedis = do ->

  SERVICE = "/api/"

  proxy = (type, method, parameters = {}, onSuccess, onError) ->
    $.ajax
      url: SERVICE + method
      type: type
      data: parameters
      dataType: 'json'
      contentType: "application/x-www-form-urlencoded"

      success: (response) ->
        onSuccess.call(onSuccess, response)
      error: (xhr, error) =>
        if onError then onError.call(onError, xhr.status, xhr.responseJSON.message)
        else
          if xhr.status isnt 0
            alert "Error #{xhr.status}: #{xhr.responseText}"

  proxy: proxy
