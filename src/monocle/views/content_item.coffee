class __View.ContentItem extends Monocle.View

  template : """
    <header class="wrap icon-present">
      <a class="icon" href="http://insideintercom.io/machine-learning-way-easier-than-it-looks/">
        <img src="//assets.monocle.io/crop/98x98/cdn.insideintercom.io/wp-content/uploads/2013/11/header.001.png">
      </a>
    <section>
      <h1 class="title">
        <a href="{{url}}">{{title}}</a>
      </h1>
      <div class="meta">
        <span class="meta-point domain">
          <a href="{{url}}">
            {{url}}
          </a>
        </span>
        <time class="meta-point discuss">
          {{created_at}}
        </time>
        <span class="meta-point user-handle">
          by <a>
            {{author}}
          </a>
        </span>
      </div>
      <p class="summary">
        {{content}}
      </p>
    </section>
    <link rel="prerender" href="{{url}}">
  </header>
  """

  constructor: ->
    super
    @html @model
