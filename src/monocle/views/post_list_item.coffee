class __View.PostListItem extends Monocle.View

  template: """

    <div class="item active">
      <section class="index">1.</section>
      <section>
        <h5 class="title">
          <a href="#/{{shortcut}}">{{title}}</a>
        </h5>
        <div class="meta">
          <span class="meta-point votes vote">25</span>
          <span class="meta-point domain">
            <a href="{{url}}">{{url}}</a>
          </span>
          <span class="meta-point user-handle">by
            <a>{{author}}</a>
          </span>
        </div>
      </section>
      <time>{{created_at}}</time>
    </div>
  """

  events:
    "click" : "onClick"

  constructor: ->
    super

  onClick: =>
    window.location.href = "#/#{@model.shortcut}"
    __Controller.Post.render @model
