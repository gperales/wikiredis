class RouterCtrl extends Monocle.Controller

  elements:
    "footer" : "footer"

  constructor: ->
    super
    @footer.hide()
    Monocle.Route.options.history = false
    @routes
      "/:shortcut"    : @_renderContent
    Monocle.Route.listen()
    unless window.location.hash then @_renderContent()

  _renderContent: (parameters={}) ->
    __Controller.Post.getPosts parameters.shortcut

$ ->
  __Controller.Router = new RouterCtrl "body"