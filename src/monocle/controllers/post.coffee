class PostCtrl extends Monocle.Controller

  elements:
    "footer"                             : "footer"
    "[name=title]"                       : "title"
    "[name=shortcut]"                    : "shortcut"
    "[name=url]"                         : "url"
    "[name=content]"                     : "content"

  events:
    "click [data-action=showCreateForm]" : "onShowCreateForm"
    "click [data-action=createPost]"     : "onCreatePost"

  constructor: ->
    super
    __Model.Post.bind "create", @bindPostCreate

  getPosts: (shortcut=null) ->
    if __Model.Post.all().length == 0
      WikiRedis.proxy "GET", "posts", {}, (response) =>
        for post in response.result
          date = new Date(parseInt(post.created_at))
          post.created_at = moment(date).fromNow()
          __Model.Post.create post
        if shortcut then @fetchOne shortcut

  bindPostCreate: (post) ->
    if not post.is_new
      view = new __View.PostListItem model: post, container: "[data-state='popular']"
      view.append post

  render: (model) ->
    new __View.ContentItem model: model, container: ".posts-show"

  fetchOne: (shortcut) ->
    post = __Model.Post.findBy "shortcut", shortcut
    @render post

  onShowCreateForm: () ->
    @footer.show()

  onCreatePost: () ->
    parameters =
      title     : @title.val()
      shortcut  : @shortcut.val()
      url       : @url.val()
      content   : @content.val()

    WikiRedis.proxy "POST", "post", parameters, (response) =>
      response.result.is_new = true
      post = __Model.Post.create response.result
      date = new Date()
      post.created_at = moment(date).fromNow()
      view = new __View.PostListItem model: post, container: "[data-state='popular']"
      view.prepend post
      for field in ["title", "shortcut", "content", "url"]
        @["#{field}"].val ""
      @footer.hide()

$ ->
  __Controller.Post = new PostCtrl "body"
