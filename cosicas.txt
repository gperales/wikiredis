
NUNCA HACER ESTO (varios clientes a la vez...)
    x = GET foo
    x = x + 1
    SET foo x
    hacer: INCR foo

LISTAS:
    LPUSH mylistKey a
    LPUSH mylistKey b
    LPUSH mylistKey c
    => (c, b, a)
    LRANGE mylist 0 1
    => (c, b)
    LRANGE mylist 0 -1
    => (c, b, a)


GENERACION DE UIDS y GUARDADO DE MODELOS

    Nunca usar SET ni GETS,
    usar HGET y HSET o HMGETALL o HGETALL

    (ejemplo para post):

        postUID = INCR global:uid:post
        HMSET post:#{postUID} title "My primer post" shortcut "my-primer-post"

        y por si queremos buscar por shortcut... (array de campos por los que queremos buscar)
        SET post:__shortcut:my-primer-post #{postUID}

    (ejemplo para user):

        userUID = INCR global:uid:user
        HMSET user:#{userUID} username pina password 12837eiud37 appnima_id xxx appnima_access_token 27364teudy823 appnima_refresh_token 23847rtwegay73



AUTENTICACION (me llega username y password):

    appnima = Yoi.Appnima.login rest.parameter("mail"), rest.parameter("password")
    HMSET user:#{userUID} appnima_id appnima.id appnima_access_token appnima.access_token appnima_refresh_token appnima.refresh_token



