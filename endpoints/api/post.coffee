"use strict"

Yoi       = require "yoi"
PostModel = require "../../commons/model/post"
Session   = require "../../commons/session"

scrape    = require 'request'
cheerio   = require 'cheerio'

module.exports = (server) ->

  ###
    Creates posts
    @method post
    @param  {string} Title from scrape method
    @param  {string} Post sortcut
    @param  {string} Post content
    @param  {string} Post author
  ###
  server.post "/api/post", (request, response) ->
    try
      rest = new Yoi.Rest request, response
      Session rest, (user) ->
        post = new PostModel()
        parameters =
          content   : rest.parameter "content"
          url       : rest.parameter "url"
          author    : user.mail

        scrape parameters.url, (error, response, html) =>
          if not error and response.statusCode is 200
            $ = cheerio.load(html)
            $("title").each (i, element) ->
              a = $(this)
              parameters.title = a.text()

            parameters.shortcut = _shortcut(parameters.title)
            post.create(parameters).save().then (error, result) ->
              rest.run result: post.parse()
    catch error
      rest.exception error.code, error.message

  ###
    Get post
    @method get
    @param  {string} Post ID
    @param  {string} Post sortcut
  ###
  server.get "/api/post", (request, response) ->
    try
      rest = new Yoi.Rest request, response
      rest.required ["shortcut"]
      Session rest, (user) ->
        post = new PostModel()
        post.getBy("shortcut", rest.parameter("shortcut")).then (error, result) ->
          rest.run result: result
    catch error
      rest.exception error.code, error.message

  ###
    Get all posts with max range
    @method get
    @param  {number} NUM_RESULTS: max range
    @param  {number} Page
  ###
  server.get "/api/posts", (request, response) ->
    try
      NUM_RESULTS = 10
      rest = new Yoi.Rest request, response
      Session rest, (user) ->
        post = new PostModel()
        if rest.parameter "page"
          pagination =
            page: rest.parameter "page"
            results: NUM_RESULTS

        post.find(pagination).then (error, result) ->
          rest.run result: result
    catch error
      rest.exception error.code, error.message

_shortcut = (title) ->
  shortcut = title.trim().replace("&","and").replace(":","_").replace(/\s/g,"_").toLowerCase()
  shortcut
