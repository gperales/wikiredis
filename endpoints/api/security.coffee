"use strict"

Yoi         = require "yoi"


module.exports = (server) ->

  server.post "/api/signup", (request, response, next) ->
    rest = new Yoi.Rest request, response
    rest.required ["mail", "password"]

    Yoi.Hope.shield([->
      Yoi.Appnima.signup rest.parameter("mail"), rest.parameter("password")
    , (error, appnima) ->
      promise = new Yoi.Hope.Promise()
      Yoi.Redis.run "hmset", "user:#{appnima.id}", _parameters(appnima)
      promise.done null, appnima
      promise
    ]).then (error, user) ->
      if error?
        rest.exception error.code, error.message
      else
        rest.run user: user

  server.post "/api/login", (request, response, next) ->
    rest = new Yoi.Rest request, response
    rest.required ["mail", "password"]

    Yoi.Hope.shield([->
      Yoi.Appnima.login rest.parameter("mail"), rest.parameter("password")
    , (error, appnima) ->
      promise = new Yoi.Hope.Promise()
      Yoi.Redis.run "hmset", "user:#{appnima.id}", _parameters(appnima)
      promise.done null, appnima
      promise
    ]).then (error, user) ->
      if error
        rest.notFound()
      else
        rest.run user: user


_parameters = (appnima) ->
  user =
    appnima_id    : appnima.id
    mail          : appnima.mail
    name          : if appnima.name? then appnima.name
    username      : appnima.username
    avatar        : appnima.avatar
    access_token  : appnima.access_token
    refresh_token : appnima.refresh_token
    expires_in    : if appnima.expires_in? then appnima.expires_in else null
  user
